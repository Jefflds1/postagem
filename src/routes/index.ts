import { Router } from "express";
import postController from "../api/controllers/Post/Post.controller";
import verifyTokenMiddleware from "../api/services/verify.token.middleware";

const routes = Router();

routes.get("/", (req, res) => {
  res.send("Bem-Vindo a Postagem API");
});

routes.get("/posts/id:", verifyTokenMiddleware.verifyToken, postController.getAllPosts)


export default routes;
